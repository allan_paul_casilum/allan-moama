@extends('admin.layouts.app')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <p>This is my body content.</p>
   
   @alert(['foo'=>'bar'])
     @slot('title')
        Forbidden
     @endslot
     <strong>Whoops!</strong> Something went wrong!
   @endalert
   
   @hello('Allan')
   
@endsection

@section('footer')
    <p>This is my footer content.</p>
    @push('scripts')
        <script src="/example.js"></script>
    @endpush
@endsection
