@extends('admin.main')

@section('title', 'User')

@section('content-header-title')
    <h1>User Dashboard</h1>
@endsection

@section('content')
    <h1>User Content</h1>
    <div class="bs-user-table table-responsive" data-example-id="hoverable-table"> 
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Name</th> 
                </tr> 
            </thead> 
            <tbody>
                 @foreach ($users as $user)
                    <tr> 
                        <th scope="row">{{$loop->iteration}}</th> 
                        <td>
                            {{$user->name}} 
                            - <a class="btn btn-primary" href="{{route('user.edit', $user)}}" role="button">Edit</a>
                        </td> 
                    </tr>
                @endforeach 
            </tbody> 
        </table> 
    </div>
@endsection
