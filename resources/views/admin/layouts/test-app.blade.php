<html>
    <head>
        <title>App Name - @yield('title')</title>
    </head>
    <body>
        @section('sidebar')
            This is the master sidebar.
        @show

        <div class="container">
            @yield('content')
        </div>
        
        <footer>
            @yield('footer')
        </footer>
        
        @stack('scripts')
    </body>
</html>
