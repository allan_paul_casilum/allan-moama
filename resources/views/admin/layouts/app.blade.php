<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Moama Bakery - @yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="static/img/favicon.ico"/>
 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ URL::asset('css/copilot.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    @stack('header-scripts')
    
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div id="root"></div>

    <div class="wrapper">
        
        <header class="main-header">
             @yield('header')
        </header>
        
        <aside class="main-sidebar">
            <section class="sidebar-wrapper">
                @yield('sidebar')
            </section>
        </aside>
        <div class="control-sidebar-bg"></div>
        
        <div class="content-wrapper">
            <section class="content-header">
                @yield('content-header-title')
                @yield('content-header')
            </section>
            <section class="content">
                @yield('content')
            </section>
            @yield('content-wrapper-end')
        </div>
        
        <footer class="main-footer">
            @yield('footer')
        </footer>
        
    </div>
    
    <!-- built files will be auto injected. Static files below -->
    <script src="{{ URL::asset('js/app.js') }}"></script>
    <script src="{{ URL::asset('js/copilot.js') }}"></script>
    
    @stack('footer-scripts')
    
  </body>
</html>
