@extends('admin.layouts.app')

@section('title', 'Main')

@section('header')
<nav class="navbar navbar-static-top">
  <div class="container-fluid">
  <div class="navbar-header">
    <a href="#" class="navbar-brand"><b>Admin</b>-Moamabakery</a>
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
      <i class="fa fa-bars"></i>
    </button>
  </div>
</nav>
@endsection

@section('content-header-title')
    <h1>Dashboard Title</h1>
@endsection

@section('content')
    <h1>Main Content</h1>
@endsection

@section('sidebar')
<div class="main-sidebar">
    <!-- Inner sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">DASHBOARD</li>
            <li class="treeview">
            <a href="#"><span>USER</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{route('user.index')}}">List</a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
      <!-- /.sidebar-menu -->
    </div><!-- /.sidebar -->
</div><!-- /.main-sidebar -->
@endsection

@section('footer')
<div class="pull-right hidden-xs">

</div>
<strong>Copyright ©  <a href="#">Fair Digital</a>.</strong> All rights reserved.
@endsection
