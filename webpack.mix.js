let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.autoload({
    jquery: ['$', 'window.jQuery',"jQuery","window.$","jquery","window.jquery"]
});
mix.js('resources/assets/js/app.js', 'public/js')
   .js([
    'resources/assets/copilot/js/plugins/jQuery/jquery.js',
    'resources/assets/copilot/js/plugins/bootstrap/bootstrap.js',
    'resources/assets/copilot/js/plugins/AdminLTE/app.min.js',
   ],'public/js/copilot.js')
   .styles([
    'resources/assets/copilot/css/bootstrap.min.css',
    'resources/assets/copilot/css/AdminLTE.min.css',
    'resources/assets/copilot/css/skin-blue.min.css',
   ],'public/css/copilot.css')
   .sass('resources/assets/sass/app.scss', 'public/css');
   
