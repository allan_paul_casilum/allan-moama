<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		//
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		//dd($request->user()->roles());
		echo 'user manager';
    }
    
    /*
	  public function someAdminStuff(Request $request)
	  {
		$request->user()->authorizeRoles('administrator');
		return view('some.view');
	  }
	*/
}
