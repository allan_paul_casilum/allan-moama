<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    
    const TIMESLOT_MORNING = 'morning';
    const TIMESLOT_LUNCH = 'lunch';
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'timeslot',
        'tax_amount',
        'total_amount',
        'discount_amount',
    ];
    
    /**
     * Relation to the products
     * */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'order_products')
            ->withPivot('quantity', 'unit_price', 'discount')
            ->withTimestamps();
    }

    /**
     * Relation to the user
     * */
    public function users()
    {
        return $this->belongsTo('App\Models\User');
    }
}
