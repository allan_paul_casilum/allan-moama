<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryLocation extends Model
{
    protected $table = 'delivery_locations';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'address1', 'address2', 'address3', 'postal', 'city', 'state', 'country', 'lat', 'lng',
    ];
    
    /**
     * Relation to the user
     * */
    public function users()
    {
        return $this->belongsTo('App\Models\User');
    }
}
