<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'unit_price',
        'available_quantity',
        'discount_rate',
    ];
    
    /**
     * Relation to the category
     * */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'category_products')->withTimestamps();
    }

    /**
     * Relation to the orders
     * */
    public function orders()
    {
        return $this->belongsToMany('App\Models\Product', 'order_products')->withTimestamps();
    }

    /**
     * Relation to the media
     * */
    public function media()
    {
        return $this->belongsToMany('App\Models\Media', 'product_media')->withTimestamps();
    }
}
