<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/**
 * This is for the category model
 * */
$factory->define(
    App\Models\Category::class, function (Faker $faker) {
        return[
            'name' => $faker->word,
        ];
    }
);

/**
 * This is for the product model
 * */
$factory->define(
    App\Models\Product::class, function (Faker $faker) {
        return[
            'name' => $faker->sentence($nbWords = 6, $variableNbWords = true),
            'unit_price' => $faker->randomDigitNotNull,
            'available_quantity' => $faker->boolean($chanceOfGettingTrue = 50),
            'discount_rate' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 70),
        ];
    }
);

/**
 * This is for the order table
 * */
$factory->define(
    App\Models\Order::class, function (Faker $faker) {
        return[
            'user_id' => App\Models\User::all()->random()->id,
            'timeslot' => $faker->randomElement([App\Models\Order::TIMESLOT_LUNCH, App\Models\Order::TIMESLOT_MORNING]),
            'tax_amount' => 0,
            'total_amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 1000),
            'discount_amount' => 0,
        ];
    }
);

/**
 * Media Model
 * 'path' => $faker->image('public/storage/product_images', $width = 640, $height = 480, null, false),
 * $faker->imageUrl($width = 640, $height = 480)
 * */
$factory->define(
    App\Models\Media::class, function (Faker $faker) {
        return[
            'name' => $faker->word,
            'type' => $faker->randomElement([1, 2]),
            'path' => $faker->image('public/storage/', $width = 640, $height = 480, 'food', false),
        ];
    }
);
