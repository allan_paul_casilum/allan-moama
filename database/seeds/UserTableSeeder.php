<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\RoleUser;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_administrator = Role::where('name', 'administrator')->first();
        factory(App\Models\User::class)->create(['name'=> 'Administrator', 
            'email' => 'admin@mail.com',
        ])->roles()->attach($role_administrator);

        $role_manager = Role::where('name', 'manager')->first();
        factory(App\Models\User::class)->create(['name'=> 'Manager', 
            'email' => 'manager@mail.com',
        ])->roles()->attach($role_manager);

        $role_customer = Role::where('name', 'customer')->first();
        factory(App\Models\User::class)->create(['name'=> 'Customer A', 
            'email' => 'customer.a@mail.com',
        ])->roles()->attach($role_customer);
        factory(App\Models\User::class)->create(['name'=> 'Customer B', 
            'email' => 'customer.b@mail.com',
        ])->roles()->attach($role_customer);
        factory(App\Models\User::class)->create(['name'=> 'Customer C', 
            'email' => 'customer.c@mail.com',
        ])->roles()->attach($role_customer);
        
    }
}
