<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Database\Eloquent\Model::unguard();
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();

        \DB::table('users')->truncate();
        \DB::table('role_user')->truncate();
        \DB::table('roles')->truncate();
        \DB::table('order_products')->truncate();
        \DB::table('orders')->truncate();
        \DB::table('delivery_locations')->truncate();
        \DB::table('category_products')->truncate();
        \DB::table('product_media')->truncate();
        \DB::table('products')->truncate();
        \DB::table('categories')->truncate();
        \DB::table('media')->truncate();

        // Role comes before User seeder here.
        $this->call(RoleTableSeeder::class);
        // User seeder will use the roles above created.
        $this->call(UserTableSeeder::class);
        //seed products
        $this->call(ProductTableSeeder::class);
        //seed orders
        $this->call(OrderTableSeeder::class);

        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();
    }
}
