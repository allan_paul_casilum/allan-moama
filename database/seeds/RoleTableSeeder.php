<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Role::class)->create(
            ['name'=>'administrator','description'=>'Administrator User']
        );
        factory(App\Models\Role::class)->create(
            ['name'=>'manager','description'=>'Manager User']
        );
        factory(App\Models\Role::class)->create(
            ['name'=>'customer','description'=>'Customer User']
        );
        factory(App\Models\Role::class)->create(
            ['name'=>'guest','description'=>'Guest User']
        );
    }
}
