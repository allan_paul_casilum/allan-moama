<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Category::class, 10)->create()->each(function ($category) {
            //attach product to category
            $category->products()->saveMany(factory(App\Models\Product::class, 20)->make())->each(function ($product){
                //attach media to products
                factory(App\Models\Media::class)->create(['type' => 1])->products()->attach($product);
            });
        });
    }
}
