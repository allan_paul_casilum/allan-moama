<?php

use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Order::class, 20)->create()->each(function ($orders) {
            $products = App\Models\Product::inRandomOrder()->take(rand(1, 5))->get();
            $orders->products()->attach($products, [
                'quantity' => rand(1, 10),
                'unit_price' => rand(100, 1000) / 100, 
                'discount_rate' => 0,
            ]);
        });
    }
}
